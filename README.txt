Archivos:

casilla.h: encabezados para las funciones pedidas.  Ademas
  se incluye la funcion error, que esta implementada en test-casilla.c.
test-casilla.c: el programa de prueba de su tarea.
salida.txt: la salida correcta del programa de prueba.  Cualquier
  diferencia en la salida significa que su tarea *no* funciona.
colapri-arreglo.c: la implementacion de las colas de prioridad de
  la tarea 2 en base a arreglos.  Usela!
colapri.h y colapri-arreglo.h: necesita incluir estos archivos
  para poder usar la cola de prioridad.
Makefile: Para que compile su tarea.

casilla.c: no incluido.  Ud. debe crearlo colocando ahi
  la implementacion de las funciones pedidas.

Note que una casilla es un caso particular del buffer del problema
del productor/consumidor.  La variante esta en que el buffer
de la casilla es de tama�o 0.
