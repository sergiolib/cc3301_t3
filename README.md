Mi tarea está publicada en este repositorio en GitHub para facilitar
el versionamiento y que no se me vaya a borrar algo. En otras
palabras, esto es por seguridad y, como no pago por esto, no lo puedo
poner privado.

No permito que nadie use este código en su propia tarea. Si alguien lo
hace (y me entero de ello), lo acusaré sin remordimientos de plagio
con los profesores de CC3301. Quedas advertido.

Para generar archivo TAGS: find . -type f -iname "*.[chS]" | xargs etags -a
